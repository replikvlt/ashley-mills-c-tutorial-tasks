#include <stdio.h>

int main(int argc, char** argv){
  double t_c=55;
  double t_f;
  t_f = t_c * 9/5 + 32;

  printf("In Double/Float: %f celsius = %f fahrenheit\n\
In Integer: %i celsius = %i fahrenheit\n",t_c,t_f,(int) t_c, (int) t_f);

	return 0;
}
