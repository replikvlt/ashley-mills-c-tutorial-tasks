#include <stdio.h>

int main(int argc, char** argv){

	printf("Enter character: ");
	int character = getc(stdin);
	
	switch(character){
		case 'a':
			printf("You typed a\n");
			break;
		case 'b':
			printf("You typed b\n");
			break;
		case 'c':
			printf("You typed c\n");
			break;
		case 'd':
			printf("You typed d\n");
			break;
		default: 
			if (character == 'e'){
				printf("You typed e\n");
			}else if (character == 'f'){
				printf("You typed f\n");
			};
			break;
	};

	return 0;
}
