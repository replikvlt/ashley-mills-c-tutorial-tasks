#include <stdio.h>

/*
 * Video 8: Functions
 * url: https://youtu.be/QLVc2J_bGQE
 * Task 1 : fibonacci function
 * fibonacci = 0 1 1 2 3 5 8...
 * index     = 1 2 3 4 5 6 7...
 * */
int fib( int max_step ){

  int num1, num2, this_step;
  num1 = 0;
  num2 = 1;
  printf("%d %d ", num1, num2);
  for (this_step = 1;this_step <= max_step; this_step++){
    num1 = num1 + num2;
    num2 = num2 + num1;
    printf ("%d %d ", num1, num2);
  }
  printf("\n");
  return 0;
}


int
main ( int argc, char *argv[] ){
  fib(5);
  fib(15);
  return 0;
}
